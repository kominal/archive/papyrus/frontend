import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent, UserGuard } from '@kominal/core-user-service-angular-client';
import { CoreComponent } from './modules/core/core.component';
import { PapyrusComponent } from './modules/papyrus/papyrus.component';

const routes: Routes = [
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: () => import('@kominal/core-user-service-angular-client').then((m) => m.AuthenticationModule),
	},
	{
		path: 'tenants',
		component: PapyrusComponent,
		loadChildren: () => import('./modules/papyrus/papyrus.module').then((m) => m.PapyrusModule),
		canActivate: [UserGuard],
	},
	{
		path: '',
		component: CoreComponent,
		loadChildren: () => import('./modules/core/core.module').then((m) => m.CoreModule),
	},
	{ path: '**', redirectTo: '' },
];
@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
