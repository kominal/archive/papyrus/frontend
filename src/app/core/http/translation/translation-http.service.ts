import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Translation } from '../../models/translation';

@Injectable({
	providedIn: 'root',
})
export class TranslationHttpService {
	constructor(private httpClient: HttpClient) {}

	list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Translation>> {
		return this.httpClient
			.get<PaginationResponse<Translation>>(`/controller/translations`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(translationId: string): Promise<Translation> {
		return this.httpClient
			.get<Translation>(`/controller/translations/${translationId}`, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	create(translation: Translation): Promise<{ _id: string }> {
		return this.httpClient
			.post<{ _id: string }>(`/controller/translations`, translation, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	update(translation: Translation): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/translations/${translation._id}`, translation, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	delete(translationId: string): Promise<void> {
		return this.httpClient
			.delete<void>(`/controller/translations/${translationId}`, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}
}
