import { TestBed } from '@angular/core/testing';

import { TranslationHttpService } from './translation-http.service';

describe('TranslationHttpService', () => {
  let service: TranslationHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TranslationHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
