import { Component } from '@angular/core';
import { UserService } from '@kominal/core-user-service-angular-client';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
	constructor(public userService: UserService) {}
}
