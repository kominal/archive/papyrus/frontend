import { Inject, Injectable, InjectionToken } from '@angular/core';
import { UserService } from '@kominal/core-user-service-angular-client';
import { DialogService } from '@kominal/lib-angular-dialog';
import { CrudWithTenantHttpService } from '@kominal/lib-angular-http';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { Translation } from '../../models/translation';

export const TRANSLATION_HTTP_SERVICE = new InjectionToken<CrudWithTenantHttpService<Translation>>('TRANSLATION_HTTP_SERVICE');

@Injectable({
	providedIn: 'root',
})
export class TranslationService {
	constructor(
		@Inject(TRANSLATION_HTTP_SERVICE) private translationHttpService: CrudWithTenantHttpService<Translation>,
		private userService: UserService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Translation>> {
		try {
			return await this.translationHttpService.list(this.userService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(translationId: string): Promise<Translation | undefined> {
		try {
			return await this.translationHttpService.get(this.userService.getCurrentTenantId(), translationId);
		} catch (e) {
			this.logService.handleError(e);
		}
		return undefined;
	}

	create(translation: Translation): Promise<{ _id: string }> {
		return this.translationHttpService.create(this.userService.getCurrentTenantId(), translation);
	}

	update(translation: Translation): Promise<void> {
		return this.translationHttpService.update(this.userService.getCurrentTenantId(), translation);
	}

	createOrUpdate(translation: Translation): Promise<void | { _id: string }> {
		return translation._id ? this.update(translation) : this.create(translation);
	}

	async delete(translation: Translation): Promise<boolean> {
		try {
			if (translation._id && (await this.dialogService.openConfirmDeleteDialog(translation._id))) {
				await this.translationHttpService.delete(this.userService.getCurrentTenantId(), translation._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
