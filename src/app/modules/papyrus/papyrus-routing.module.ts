import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslationComponent } from './pages/translation/translation.component';
import { TranslationsComponent } from './pages/translations/translations.component';

const routes: Routes = [
	{ path: ':tenantId/create', component: TranslationComponent },
	{ path: ':tenantId/:translationId', component: TranslationComponent },
	{ path: ':tenantId', component: TranslationsComponent },
	{ path: '**', redirectTo: '/' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PapyrusRoutingModule {}
