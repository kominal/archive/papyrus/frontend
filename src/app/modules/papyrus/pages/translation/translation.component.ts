import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseTenant, TenantService } from '@kominal/core-user-service-angular-client';
import { LogService } from '@kominal/lib-angular-logging';
import { RouterService } from '@kominal/lib-angular-router';
import { BehaviorSubject } from 'rxjs';
import { TranslationService } from 'src/app/core/services/translation/translation.service';

@Component({
	selector: 'app-translation',
	templateUrl: './translation.component.html',
	styleUrls: ['./translation.component.scss'],
})
export class TranslationComponent {
	public Object = Object;
	public translationTypeSubject = new BehaviorSubject<string[]>(['SINGLE', 'TEXT', 'MULTI']);

	public loading = true;
	public form: FormGroup;

	public projectNamesSubject = new BehaviorSubject<any[]>(['abc', 'def']);

	public values: any = new FormArray(
		['en', 'de'].map((language) => {
			return new FormGroup({
				language: new FormControl(language, Validators.required),
				value: new FormControl(),
			});
		})
	);

	constructor(
		private translationService: TranslationService,
		private logService: LogService,
		private tenantService: TenantService<BaseTenant>,
		private routerService: RouterService,
		activatedRoute: ActivatedRoute
	) {
		this.form = new FormGroup({
			_id: new FormControl(),
			projectName: new FormControl(undefined, Validators.required),
			type: new FormControl('SINGLE', Validators.required),
			key: new FormControl(undefined, Validators.required),
			values: this.values,
			global: new FormControl(),
		});
		this.form.controls.global.valueChanges.subscribe(async (type) => this.updateInternalValidators(type));
		this.updateInternalValidators();

		activatedRoute.params.subscribe(async ({ translationId }) => {
			if (translationId) {
				const translation = await translationService.get(translationId);
				if (translation) {
					this.form.patchValue(translation);
					this.form.controls.global.setValue(translation.projectName === undefined || translation.projectName === null);

					translation.values.forEach(({ language, value }) => this.values.get(language)?.setValue(value));
				} else {
					this.routerService.back(this.tenantService.getTenantUrl(['/translations']));
				}
			}
			this.loading = false;
		});
	}

	updateInternalValidators(global?: boolean) {
		this.form.controls.projectName.clearValidators();

		if (!global) {
			this.form.controls.projectName.setValidators(Validators.required);
		}

		this.form.controls.projectName.updateValueAndValidity();
	}

	async onSubmit() {
		this.loading = true;
		try {
			const value = this.form.value;
			if (value.global) {
				value.projectName = null;
			}
			await this.translationService.createOrUpdate(value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.routerService.back(this.tenantService.getTenantUrl([]));
	}
}
