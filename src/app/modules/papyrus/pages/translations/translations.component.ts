import { Component, ViewChild } from '@angular/core';
import { UserService } from '@kominal/core-user-service-angular-client';
import { TableComponent } from '@kominal/lib-angular-table';
import { Translation } from 'src/app/core/models/translation';
import { TranslationService } from 'src/app/core/services/translation/translation.service';

@Component({
	selector: 'app-translations',
	templateUrl: './translations.component.html',
	styleUrls: ['./translations.component.scss'],
})
export class TranslationsComponent {
	@ViewChild(TableComponent) table!: TableComponent<Translation>;

	Object = Object;

	autoColumns = ['projectName', 'key'];
	displayedColumns = ['projectName', 'languages', 'key', 'type', 'actions'];

	constructor(public translationService: TranslationService, public userService: UserService) {}

	async delete(translation: Translation) {
		if (await this.translationService.delete(translation)) {
			this.table.triggerUpdate();
		}
	}
}
