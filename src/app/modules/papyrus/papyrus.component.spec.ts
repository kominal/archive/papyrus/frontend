import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PapyrusComponent } from './papyrus.component';

describe('PapyrusComponent', () => {
  let component: PapyrusComponent;
  let fixture: ComponentFixture<PapyrusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PapyrusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PapyrusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
