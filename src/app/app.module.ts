import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreUserServiceModule } from '@kominal/core-user-service-angular-client';
import { CrudWithTenantHttpService, HttpModule } from '@kominal/lib-angular-http';
import { RouterModule, RouterService } from '@kominal/lib-angular-router';
import { PapyrusClientModule } from '@kominal/papyrus-angular-client';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/components/header/header.component';
import { TRANSLATION_HTTP_SERVICE } from './core/services/translation/translation.service';
import { MaterialModule } from './shared/material.module';

@NgModule({
	declarations: [AppComponent, HeaderComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MaterialModule,
		HttpModule,
		RouterModule,
		PapyrusClientModule.forRoot({ tenantId: '601ae42c656c657164122b24', projectName: 'papyrus', languages: ['de', 'en'] }),
		CoreUserServiceModule.forRoot(),
	],
	providers: [
		{
			provide: APP_INITIALIZER,
			useFactory: () => () => {},
			deps: [RouterService],
			multi: true,
		},
		{
			provide: TRANSLATION_HTTP_SERVICE,
			useFactory: (httpClient: HttpClient) => new CrudWithTenantHttpService(httpClient, '/controller', 'translations'),
			deps: [HttpClient],
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
